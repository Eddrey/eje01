<%-- 
    Document   : salida
    Created on : 30-03-2020, 21:00:17
    Author     : Lewiss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>Ejercicio 01</title>
    </head>
    <body>
       <h1>El resultado es:</h1>
        <%
            String nombre = (String) request.getAttribute("nombre");
            String seccion = (String) request.getAttribute("seccion");
        %>
        
        <p>Hola <%=nombre%>, estás en la sección <%=seccion%></p>
    </body>
</html>
